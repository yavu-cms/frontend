#!/bin/bash

general_status=""
for stage_config in config/deploy/*.rb; do
  stage=$(basename $stage_config .rb)
  echo "DEPLOYING $stage ..."
  bundle exec cap $stage deploy
  status=$?
  general_status="$general_status\n $stage status was: $status"
done;

echo $general_status
