# Ejemplo de Yavu frontend

Este proyecto es un ejemplo de cómo crear una aplicación frontend para el CMS YAVU

## Introducción

Este proyecto no es una simple aplicación WEB sino que además provee utiliza servicios propios que permiten comunicación en línea con el backend

Dado que las aplicaciones sinatra no proveen una consola *a la Rails*, se provee una consola propia accesible usando `racksh` que puede utilizarse con fines de debug

# Instalación

Los pasos a seguir para instalar un frontend son los mismos que para el backend, considerando que el único prerequisito que **no utiliza** motor de base de datos ni mongoDB.

## Instalando los prerequisitos

* Seguir los pasos indicados para el [backend](https://git.cespi.unlp.edu.ar/desarrollo/el-dia-backend/tree/development)
  * Los pasos son similares salvo, como se indica arriba, los relacionados al motor de base de datos y MongoDB

## Instalando la componente del Frontend

En la estación de trabajo del administrador, clonamos el repositorio del frontend con el siguiente comando:

```bash
git clone https://git.cespi.unlp.edu.ar/desarrollo/yavu-frontend-sample.git
cd backend
bundle
```

### Utilizando autenticación

Ver la documentación en `docs/README.auth.es.md`

## Configuración del Frontend

La configuración completa del producto depende de:

* Configuración general: `config/config.rb`
* Meta configuraciones a partir de variables de ambiente: `.env`
* Servicios
  * Configuracion del servidor WEB: `config/puma-config.rb`
* Configuración de inicio de los servicios: `Procfile`


## Configuración del deploy con capistrano

Al igual que en el backend, la configuración de capistrano se realiza mediante el archivo `config/deploy.rb` pero además, se personaliza la configuración de diferentes servidores usando archivos en el directorio `config/deploy`.

Asumimos que el servidor a instalar se llama production, entonces creamos `config/deploy/production.rb` con el siguiente contenido:

```ruby
set :application, 'frontend'
set :deploy_to, "/home/#{application}"
set :user, application
set :domain, 'IP_SERVER'
server domain, :app, :web, :db, :primary => true
```

Una vez creado el archivo, podremos avanzar el deploy con capistrano.

## Setup inicial con capistrano

Corremos:

```bash
bundle exec cap production deploy:setup
```

Esto instalará las dependencias en el servidor y creará una estructura de directorios en donde hayamos especificado que se instale la aplicación con la siguiente estructura:

```bash
+ ROOT
    |--- releases/
    |--+ shared/
        |--+ files/
        |--+ log/
        |--+ tmp/
```

Al finalizar el comando cap production deploy:setup debemos subir por única vez el archivo:

```
scp config/config.rb frontend@IP_SERVER:/home/frontend/shared/files/config/config.rb
scp config/puma-config.rb frontend@IP_SERVER:/home/frontend/shared/files/config/puma-config.rb
scp .env-sample frontend@IP_SERVER:/home/frontend/shared/.env
```

Nos conectamos al servidor y editamos el archivo `shared/.env` personalizando su contenido:

```bash
BASE_PORT=9292
PORT=$BASE_PORT
RACK_ENV=production

# # Disables assets & media serving
# DISABLE_STATIC_CONTENT=1

# Session attributes - match them with the users application to share user's session
SESSION_KEY=_yavu_session
SESSION_SECRET=4dd7bf397eb699aaa4acbe7900eb035af1dec0f3a2b00c103bb155bf6b3c8426a7ccfca7e18f6de543b98d49b3b737ee862e955e8884a45944f4d90c881a13f5
# # Leave the following line commented if unsure -- it defaults to :all
# SESSION_DOMAIN=.yavu.org

# Backend's API base URL
API_BASE=http://localhost:3000
CHANNEL=client-frontend-channel
NAMESPACE=yavu-backend
```

Recordar que todos los comandos anteriores se corren en el servidor con el usuario frontend

Los parámetros **fundamentales** para que funcione el frontend son:
 * API_BASE: URL de la API del backend

## Instalación final

Corremos en la estación de trabajo del administrador:

```bash
bundle exec cap production deploy
```

Finalmente podremos acceder al servidor con [http://IP_SERVER:9292](http://ip_server:9292)

# Configuración de los servicios Ruby

El backend provee dos servicios:

* Web server basado en Puma
* Un monitor que recibe señales desde el backend para recargar la configuración de los frontends

## El servidor WEB

El servidor ruby empleado es [Puma](https://github.com/puma/puma), y la configuración del  backend puede cambiarse editando `config/puma-config.rb`

## Todos los servicios

Todos los servicios se inician usando foreman según se indica en el archivo `Procfile`