class User
  attr_reader :id, :db_id, :username, :full_name, :email, :profile, :can_comment

  def self.from_session(store, id_key: 'User', username_key: 'username', full_name_key: 'name', email_key: 'email', profile_key: 'profile', db_id_key: 'db_id', can_comment_key: 'can_comment')
    new store[id_key], store[username_key], store[full_name_key], store[email_key], store[db_id_key], store[can_comment_key], store[profile_key]
  end

  def initialize(id, username, full_name, email, db_id, can_comment, profile = nil)
    @id = id
    @db_id = db_id
    @username = username
    @full_name = full_name
    @email = email
    @can_comment = can_comment
    @profile = profile || {}
  end

  def to_hash
    {uid: username, full_name: full_name, username: username, email: email, db_id: db_id, can_comment: can_comment}.merge profile
  end
end
