require 'bundler'
Bundler.require
Dotenv.load
require 'logger'

require 'yavu/frontend/server'
require 'shield'
require_relative 'models/user'

class Main < Yavu::Frontend::Server::Base
  helpers Shield::Helpers

  set :root_dir, File.join(File.dirname(__FILE__))
  set :manifest_path,  File.join(File.dirname(__FILE__), 'tmp', 'manifest.json')
  set :ill_status,     ENV['ILL_STATUS'].to_i if ENV['ILL_STATUS'] =~ /^\d+$/

  # Restart Puma Server when a misconfiguration error is detected
  error Yavu::Frontend::Server::MisconfiguredServerError do
    puts "Restarting Puma server due a misconfiguration problem!"
    Process.kill 'SIGUSR2', IO.read(File.join(Main.root_dir, 'tmp', 'pids', 'puma.pid')).chomp.to_i
  end

  helpers do
    # Helper that wraps shield authenticated. This helper is used by Yavu::Frontend::Server::Base
    # and by default returns nil
    def authenticated_user
      @_current_user ||= User.from_session(session) if session['User'].present? && session['username'].present?
    end
  end
end
