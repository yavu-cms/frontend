require_relative 'identifiers'
Yavu::Frontend::Util::ServerMonitor.configure do |config|
  # Identification for this client. Commonly is hostname:port of frontend server
  # but can be any string that identifies this service so backend server can configure
  # us remotely . We use BASE_PORT instead of PORT because when Puma uses more than 1 worker
  # it increments PORT for each worker
  config.identifier = Identifiers.identifier

  # Client application notification threshold
  config.threshold = ENV.fetch('NOTIFICATIONS_THRESHOLD') { 191 }.to_i

  # Redis server. Must be shared with backend server.
  # Must match backend_server/config/settings.yml
  #   redis:
  #     url: THIS VALUE
  config.redis_url = ENV['REDIS_URL']

  # Redis namespace to use to communicate with backend server.
  # Must match backend_server/config/settings.yml
  #   redis:
  #     namespace: THIS VALUE
  config.namespace = ENV['NAMESPACE'] || 'yavu-backend'

  # Channel used to talk with backend by redis
  # Muts match backend_server/conig/settings.yml
  #   redis:
  #     client_frontend_channel: THIS VALUE
  config.channel = ENV['CHANNEL'] || 'client-frontend-channel'

  # File where token is supposed to be read. Token is a string to watch if match to signal process
  # It is the Backend client_application model identifier that this frontend server serves
  client_id_path = Identifiers.client_id_file
  config.token = File.expand_path client_id_path, File.join(__FILE__,'../..')

  # Comment the following line to use STDOUT as default logging output
  # config.log_file = 'log/monitor.log'
end
