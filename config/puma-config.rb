#!/usr/bin/env puma

pidfile 'tmp/pids/puma.pid'

bind "unix://#{ENV['PUMA_SOCKET']}" if ENV['PUMA_SOCKET']
port ENV['PORT'] if ENV['PORT'] && ENV['PUMA_USE_PORT']
port 9292 unless ENV['PUMA_SOCKET'] || ENV['PORT']

workers Integer(ENV['PUMA_WORKERS'] || 3)
threads Integer(ENV['MIN_THREADS']  || 4), Integer(ENV['MAX_THREADS'] || 16)

preload_app!

rackup      DefaultRackup
environment ENV['RACK_ENV'] || 'development'

on_worker_boot do
  RestClient.enable ::Rack::Cache, verbose: ENV['RACK_ENV'] == 'development',
                                 metastore: ENV['REDIS_METASTORE'],
                                 entitystore: ENV['REDIS_ENTITYSTORE'],
                                 allow_reload: true,
                                 allow_revalidate: true
end

BEGIN {
  require File.expand_path 'main',    File.join(__FILE__, '..', '..')
  require 'restclient/components'
  require 'fileutils'
  require_relative 'identifiers'

  FileUtils.rm_rf File.expand_path(File.join(__FILE__, '..', '..', 'tmp', 'cache'))
  begin
    ::Main.configure do |app|
        begin
          client_id_path = Identifiers.client_id_file
          app.set :client_id, (Identifiers.configured_client_id || IO::read(client_id_path).chomp)
        rescue Errno::ENOENT => e
          puts "No client application assigned. Waiting that configuration from backend (you manually can do it creating a client.id file with the client application identifier hash"
          RestClient.post "#{ENV['API_BASE']}/api/alive", { frontend_identifier: Identifiers.identifier }
          sleep(10)
          retry
        end

        RestClient.log = nil
        app.logger.level = Logger.const_get String(ENV['LOG_LEVEL'] || 'info').upcase
        app.set :locale, 'es'
        app.set :session_key, ENV['SESSION_KEY'] || 'yavu-session'
        app.set :session_secret, ENV['SESSION_SECRET'] || 'my-secret'
        app.set :session_domain, ENV['SESSION_DOMAIN'] || :all
        app.set :api_base, ENV['API_BASE'] || 'http://localhost:3000'
        app.disable :static_backend_content if ENV['DISABLE_STATIC_CONTENT']
    end
    ::Main.configure_client!
    RestClient.post "#{ENV['API_BASE']}/api/alive", { frontend_identifier: Identifiers.identifier, client_identifier: ::Main.settings.client_id }
  rescue Errno::ECONNREFUSED, RestClient::Exception
    RestClient.post "#{ENV['API_BASE']}/api/alive", { frontend_identifier: Identifiers.identifier, client_identifier: ::Main.settings.client_id }
    sleep(10)
    retry
  end
}
