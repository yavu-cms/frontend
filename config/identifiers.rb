require 'socket' # So we get hostname

class Identifiers
  class << self
    def identifier
      "#{ENV['FRONTEND_NAME'] || Socket.gethostname}:#{ENV['BASE_PORT']}"
    end

    def client_id_file
      if ENV['CLIENT_ID_NAME'].present?
        "client/#{ENV['CLIENT_ID_NAME']}"
      else
        "client/#{identifier}-client-app.id"
      end
    end

    def configured_client_id
      ENV['CLIENT_ID'].presence
    end
  end
end
