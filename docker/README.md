# Docker YAVU Frontend

Este proyecto consta de las siguientes imágenes:

  * `registry.desarrollo.unlp.edu.ar:5000/desarrollo-public/yavu-frontend-web`: Instancia `web` de la aplicación frontend
  * `registry.desarrollo.unlp.edu.ar:5000/desarrollo-public/yavu-frontend-worker`: Instancia `worker` de la aplicación frontend

## Dependencias

* **Rocker** ([GitHub](https://github.com/grammarly/rocker))

## Builds automáticas

### Preparación

Opcionalmente, exportar la variable de ambiente `GIT_SSH_KEY` con el path absoluto a la clave privada SSH que se desea
usar para clonar el repositorio Git privado, **sólo si el valor es diferente de `~/.ssh/id_rsa`**.

```console
$ export GIT_SSH_KEY=~/.ssh/id_rsa
```

### Construir imágenes con Rocker

```console
$ ./build -v 1.1.0 -t v1.1.0
```

donde `1.1.0` es el tag a crear de docker, mientras que v1.1.0 es el tag en git del código a empaquetar.

> Notar que este comando sube automáticamente las nuevas imágenes a la registry de Docker.

## Builds manuales

### Preparación

Se debe exportar la variable de ambiente `GIT_SSH_KEY` con el path absoluto a la clave privada SSH que se desea usar
para clonar el repositorio Git privado. Típicamente, el valor será `~/.ssh/id_rsa`:

```console
$ export GIT_SSH_KEY=~/.ssh/id_rsa
```

### Construir imágenes con Rocker

* *Build arguments* disponibles:
  * `GIT_REF`: referencia a un objeto Git del repo, para fijar el código obtenido al construir la imagen a esa versión.
* Variables disponibles:
  * `version`: tag Docker a crear para ambas imágenes.

```console
$ rocker build --var version=1.0.0 --build-arg GIT_REF=d4e3b1c60 --push
```

> Notar que `--push` sube automáticamente las nuevas imágenes a la registry de Docker.
