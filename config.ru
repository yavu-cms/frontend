require File.expand_path 'main', File.join(__FILE__, '..')

use Rack::Deflater

PumaWorkerKiller.config do |config|
  # Suggested value for PUMA_RAM = 250 * PUMA_WORKERS
  config.ram = Integer(ENV['PUMA_RAM'] || 300)
  config.frequency = 60
end

PumaWorkerKiller.start

run Main
